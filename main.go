package main

import (
	"io/ioutil"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
)

const SysDir = "/sys/class/backlight/intel_backlight"
const MaxPath = "max_brightness"
const CtrlPath = "brightness"
const StepPercent = 10

func main() {
	if len(os.Args) != 2 {
		log.Fatalf("Usage: %s inc|dec", os.Args[0])
	}

	var err error

	switch os.Args[1] {
	case "inc":
		err = increase(StepPercent)
	case "dec":
		err = decrease(StepPercent)
	default:
		log.Fatalf("Unknown argument: %s", os.Args[1])
	}

	if err != nil {
		log.Fatal(err)
	}
}

func readint(path string) (int, error) {
	data, err := ioutil.ReadFile(path)
	if err != nil {
		return -1, err
	}

	return strconv.Atoi(strings.TrimSpace(string(data)))
}

func max() int {
	n, err := readint(path.Join(SysDir, MaxPath))
	if err != nil {
		panic(err)
	}
	return n
}

func current() int {
	n, err := readint(path.Join(SysDir, CtrlPath))
	if err != nil {
		panic(err)
	}
	return n
}

func setLevel(level int) error {
	if level > max() {
		level = max()
	}

	f, err := os.OpenFile(
		path.Join(SysDir, CtrlPath),
		os.O_WRONLY,
		0644,
	)
	if err != nil {
		return err
	}

	_, err = f.WriteString(strconv.Itoa(level))
	return err
}

func increase(step int) error {
	level := current() + step
	if level > max() {
		level = max()
	}

	return setLevel(level)
}

func decrease(step int) error {
	level := current() - step
	if level < 0 {
		level = 0
	}

	return setLevel(level)
}
